$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "callbaq/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "callbaq"
  s.version     = Callbaq::VERSION
  s.authors     = ["Sam Harnack"]
  s.email       = ["sam@harnackstudios.com"]
  s.homepage    = "https://sparkstream.co/callbaq"
  s.summary     = "Callback engine for rails"
  s.description = "Callback engine for rails"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]
  s.test_files = Dir["spec/**/*"]

  s.add_dependency "rails", "~> 5.0.0", ">= 5.0.0.1"

  s.add_development_dependency "pg"
  s.add_development_dependency 'rspec-rails'
  s.add_development_dependency 'capybara'
  s.add_development_dependency 'factory_girl_rails'
end
