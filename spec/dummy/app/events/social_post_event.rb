class SocialPostEvent < Callbaq
  def process *params
    # we can trigger a SwitchboardEvents.instrument here if we want
    # return false to keep the job in the queue, anything else will remove the callback
  end
end