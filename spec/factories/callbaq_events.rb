FactoryGirl.define do
  factory :callbaq_event, class: 'Callbaq::Event' do
    event "MyString"
    source "MyString"
    params ""
  end
end
