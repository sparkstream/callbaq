require 'rails_helper'

class Callbaq
  RSpec.describe Event, type: :model do
    let(:post) { Post.create }
    let(:social_post_event) { double }
    let(:params) { [:test, twitter: true, user: 123] }
    let(:event) { Callbaq::Event.first }

    before { expect(SocialPostEvent).to receive(:new).with(post, :processed).and_return social_post_event }
    before { expect(social_post_event).to receive(:process).with(*params.as_json) }
    before { SocialPostEvent.on :processed, post, *params }

    it do
      expect(Callbaq::Event.all.size).to eq 1
      expect(event.callback_class).to eq 'SocialPostEvent'
      expect(event.params).to eq params.as_json
      expect(event.event).to eq 'processed'
      expect(event.source).to eq post.to_global_id.to_s

      Callbaq.trigger :processed, post

      expect(Callbaq::Event.all.size).to eq 0
    end
  end
end
