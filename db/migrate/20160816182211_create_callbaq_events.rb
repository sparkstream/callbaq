class CreateCallbaqEvents < ActiveRecord::Migration[5.0]
  def change
    enable_extension 'uuid-ossp'

    create_table :callbaq_events, id: :uuid do |t|
      t.string :callback_class
      t.string :event
      t.string :source
      t.json :params

      t.timestamps
    end
  end
end
