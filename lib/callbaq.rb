require 'callbaq/engine'

class Callbaq
  class << self
    def on event, model, *args
      Callbaq::Event.create callback_class: self.name, event: event, source: model.to_global_id.to_s, params: args
    end

    def trigger event, model
      Callbaq::Event.where(event: event, source: model.to_global_id.to_s).find_each do |callback|
        callback.destroy unless false == callback.callback_class.constantize.new(model, event).process(*callback.params)
      end
    end
  end

  attr_reader :model

  def initialize model, event
    @model = model
    @event = event
  end

  def process *args; end
end
